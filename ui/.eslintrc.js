module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["react-app", "react-app/jest"],
  // extends: ["plugin:react/recommended"],
  parserOptions: {
    parser: "babel-eslint"
  },
  // plugins: ["prettier"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
    // "prettier/prettier": ["error"]
  }
};
