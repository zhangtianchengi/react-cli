import React, { Component } from "react";
import RouterView from "@/route/RouterView";
import Layout from "@/components/Layout/Layout";
class Index extends Component {
  render() {
    return (
      <Layout>
        <RouterView routes={this.props.routes}></RouterView>
      </Layout>
    );
  }
}

export default Index;
