import React, { Component } from "react";
import "./login.css";
import { _login } from "@/api/api.js";
import { Form, Icon, Input, Button, Card } from "antd";
class Login extends Component {
  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const res = await _login(values);
        console.log(res);
        if (res.code === 200) {
          window.sessionStorage.userInfo = JSON.stringify(res.userInfo);
          this.props.history.replace("/index/home");
        } else {
          alert(res.msg);
        }
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className='login'>
        <button onClick={() => this.ok()}>asdad</button>
        <Card
          title={<b className='title'>前端面试页</b>}
          style={{ width: 350 }}
          className=''
        >
          <Form onSubmit={() => this.handleSubmit()} className='login-form'>
            <Form.Item>
              {getFieldDecorator("uname", {
                rules: [
                  { required: true, message: "Please input your username!" }
                ]
              })(
                <Input
                  prefix={
                    <Icon type='user' style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder='Username'
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("pwd", {
                rules: [
                  {
                    required: true,
                    message: "Please confirm your password!"
                  }
                ]
              })(
                <Input.Password
                  prefix={
                    <Icon type='lock' style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder='Password'
                />
              )}
            </Form.Item>

            <Form.Item>
              <Button
                type='primary'
                htmlType='submit'
                className='login-form-button'
              >
                登录
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}

export default Form.create({ name: "normal_login" })(Login);
