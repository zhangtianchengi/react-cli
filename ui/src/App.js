import React, { Suspense } from "react";
import RouterView from "./route/RouterView";
function App() {
  return (
    <Suspense fallback={<h1>loading...</h1>}>
      <RouterView></RouterView>
    </Suspense>
  );
}
export default App;
