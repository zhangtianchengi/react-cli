import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "@/assets/css/init.css";
import { BrowserRouter } from "react-router-dom";
import ComponentDidCatch from "@/components/ComponentDidCatch/ComponentDidCatch.jsx";

ReactDOM.render(
  <ComponentDidCatch>
    <BrowserRouter>
      <App></App>
    </BrowserRouter>
  </ComponentDidCatch>,
  document.getElementById("root")
);
