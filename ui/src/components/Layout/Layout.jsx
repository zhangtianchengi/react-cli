import React, { Component } from "react";
import "./layout.css";
import { Icon, Menu } from "antd";
import { NavLink } from "react-router-dom";
const { SubMenu } = Menu;
export default class Layout extends Component {
  rootSubmenuKeys = ["sub1", "sub2", "sub4"];

  state = {
    openKeys: ["sub1"],
  };

  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(
      (key) => this.state.openKeys.indexOf(key) === -1
    );
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };

  render() {
    return (
      <div className="layout">
        <header>
          <div className="header_l">
            <h1 className="title">前端面试页</h1>
          </div>
          <div className="header_r">
            <Icon type="global" />
            <Icon type="bell" />
            <Icon type="user" />
            <span>asd</span>
          </div>
        </header>
        <main>
          <div className="main_l">
            <Menu
              mode="inline"
              openKeys={this.state.openKeys}
              onOpenChange={this.onOpenChange}
              defaultSelectedKeys="0"
            >
              <Menu.Item key="0">
                <NavLink to="/index/home">
                  {" "}
                  <Icon type="home" />
                  首页
                </NavLink>
              </Menu.Item>
              <SubMenu
                title={
                  <span>
                    <Icon type="appstore" />
                    <span>菜单1</span>
                  </span>
                }
              >
                <Menu.Item>
                  <NavLink to="/index/menu1">子菜单1</NavLink>
                </Menu.Item>
                <Menu.Item>
                  <NavLink to="/index/menu2">子菜单2</NavLink>
                </Menu.Item>
              </SubMenu>
              <SubMenu
                title={
                  <span>
                    <Icon type="setting" />
                    <span>菜单2</span>
                  </span>
                }
              >
                <Menu.Item>
                  <NavLink to="/index/menu3">子菜单3</NavLink>
                </Menu.Item>
                <SubMenu title="子菜单4">
                  <Menu.Item>
                    <NavLink to="/index/menu5">子菜单5</NavLink>
                  </Menu.Item>
                </SubMenu>
              </SubMenu>
            </Menu>
          </div>
          <div className="main_r">{this.props.children}</div>
        </main>
      </div>
    );
  }
}
