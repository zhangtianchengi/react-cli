import React from "react";
import routerTable from "./routerTable";
import { Redirect, Route, Switch } from "react-router-dom";
export default function RouterView(props) {
  const routes = props.routes || routerTable;
  return (
    <Switch>
      {routes.map((item) => {
        return item.component ? (
          <Route
            path={item.path}
            key={item.name}
            render={(routerConfig) => {
              const ItemComponent = item.component;
              return (
                <ItemComponent
                  {...routerConfig}
                  routes={item.children ? item.children : []}
                ></ItemComponent>
              );
            }}
          ></Route>
        ) : (
          <Redirect
            from={item.path}
            to={item.redirect}
            exact
            key={item.path}
          ></Redirect>
        );
      })}
    </Switch>
  );
}
