import { lazy } from "react";
const routerTable = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/index",
    name: "index",
    component: lazy(() => import("../pages/Index/Index")),
    children: [
      {
        path: "/index",
        redirect: "/index/home",
      },
      {
        path: "/index/home",
        name: "home",
        component: lazy(() => import("../pages/Index/Home/Home")),
      },
      {
        path: "/index/menu1",
        name: "menu1",
        component: lazy(() => import("../pages/Index/Menu1/Menu1")),
      },
      {
        path: "/index/menu2",
        name: "menu2",
        component: lazy(() => import("../pages/Index/Menu2/Menu2")),
      },
      {
        path: "/index/menu3",
        name: "menu3",
        component: lazy(() => import("../pages/Index/Menu3/Menu3")),
      },
      {
        path: "/index/menu5",
        name: "menu5",
        component: lazy(() => import("../pages/Index/Menu5/Menu5")),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: lazy(() => import("../pages/Login/Login")),
  },
];
export default routerTable;
